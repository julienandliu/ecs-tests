import jsoncompare
import socket
import json

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('localhost', 9000))

#create the project first
# send_to_client = {
#     "userID": "lisa",
#     "commandName": "create",
#     "objectType": "project",
#     "objectName": "testProjectBuild",
#     "additionalParameters": []
# }

#convert json to string
# client_socket.send(json.dumps(send_to_client) + '\n')
# client_socket.recv(1024)

send_to_client = {
    "userID": "lisa",
    "commandName": "compile",
    "objectType": "project",
    "objectName": "testProjectBuild",
    "additionalParameters": []
}

client_socket.send(json.dumps(send_to_client) + '\n')
response_received = client_socket.recv(1024).decode('utf-8')
print response_received
