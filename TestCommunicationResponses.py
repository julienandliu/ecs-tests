import jsoncompare
import socket
import json
import unittest


class TestChangeProjectName(unittest.TestCase):

    ############################
    #  Project related tests   #
    ############################

    def testDeleteProject(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testDeleteProject",
            "additionalParameters": []
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # delete the project name
        send_to_client = {
            "requestID": "lisa",
            "commandName": "delete",
            "objectType": "project",
            "objectName": "testDeleteProject",
            "additionalParameters": []
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                    "requestID":"lisa",
                    "responseCode":200,
                    "responseComment":"Project testDeleteProject deleted",
                    "responseContent":""
                }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testCreateProject(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testCreateProject",
            "additionalParameters": []
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
            "requestID": "lisa",
            "responseCode": 200,
            "responseComment": "Project Created Successfully",
            "responseContent": "/Users/Lisa/Desktop/workspace/testCreateProject"
        }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testChangeProjectName(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeProjectName",
            "additionalParameters": []
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the project name
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "project",
            "objectName": "testChangeProjectName",
            "additionalParameters": ['name', 'testChangeProjectName2']
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client)+'\n')

        expected_response = """{
            "requestID":"lisa",
            "responseCode":200,
            "responseComment":"Project renamed from: testChangeProjectName to: testChangeProjectName2",
            "responseContent":"/Users/Lisa/Desktop/workspace/testChangeProjectName2"
        }"""

        response_received = client_socket.recv(1024).decode('utf-8')

        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testGetProjectStructure(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testGetProjectStructure",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testGetProjectStructure"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the class
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testGetProjectStructure", "com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # Retrieve the project structure
        send_to_client = {
            "requestID": "lisa",
            "commandName": "retrieve",
            "objectType": "project",
            "objectName": "testGetProjectStructure",
            "additionalParameters": ["structure"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                         "requestID":"lisa",
                         "responseCode":200,
    "projectStructure":{
       "name":"testGetProjectStructure",
       "path":"/testGetProjectStructure",
       "type":"Folder",
       "children":[
          {
             "name":".classpath",
             "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/.classpath",
             "type":"Metadata"
          },
          {
             "name":".project",
             "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/.project",
             "type":"Metadata"
          },
          {
             "name":"bin",
             "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/bin",
             "type":"Folder",
             "children":[
             ]
          },
          {
             "name":"src",
             "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src",
             "type":"Folder",
             "children":[
                {
                   "name":"com",
                   "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src/com",
                   "type":"Folder",
                   "children":[
                      {
                         "name":"programcreek",
                         "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src/com/programcreek",
                         "type":"Folder",
                         "children":[
                            {
                               "name":"is",
                               "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src/com/programcreek/is",
                               "type":"Folder",
                               "children":[
                                  {
                                     "name":"fun",
                                     "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src/com/programcreek/is/fun",
                                     "type":"Folder",
                                     "children":[
                                        {
                                           "name":"NewClass.java",
                                           "path":"/Users/Lisa/Desktop/workspace/testGetProjectStructure/src/com/programcreek/is/fun/NewClass.java",
                                           "type":"Class"
                                        }
                                     ]
                                  }
                               ]
                            }
                         ]
                      }
                   ]
                }
             ]
          }
       ]
    }
                     }"""

        response_received = client_socket.recv(4096).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    ############################
    #  Package related tests   #
    ############################

    def testCreatePackage(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testCreatePackage",
            "additionalParameters": []
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testCreatePackage"]
        }

        # convert json to string
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Package com.programcreek.is.fun created in project testCreatePackage",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testCreatePackage/src/com/programcreek/is/fun"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testChangePackageName(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangePackageName",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testChangePackageName"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the package name
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["name", "com.programcreek.lol", "testChangePackageName"]
        }

        # send the package name change request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Package com.programcreek.is.fun successfully renamed to com.programcreek.lol in project testChangePackageName",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testChangePackageName/src/com/programcreek/lol"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testDeletePackage(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testDeletePackage",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testDeletePackage"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # delete the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "delete",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testDeletePackage"]
        }

        # send the package name change request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Package com.programcreek.is.fun deleted",
                        "responseContent":""
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')

        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    ############################
    #  Class related tests     #
    ############################

    def testCreateClass(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testCreateClass",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testCreateClass"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the class the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testCreateClass","com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Class NewClass.java created in package com.programcreek.is.fun in project testCreateClass",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testCreateClass/src/com/programcreek/is/fun/NewClass.java"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testChangeClassName(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeClassName",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testChangeClassName"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the class
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testChangeClassName","com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the class name
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["name", "LisaNewClassYay", "com.programcreek.is.fun", "testChangeClassName"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Class NewClass.java renamed successfully to LisaNewClassYay.java in package com.programcreek.is.fun",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testChangeClassName/src/com/programcreek/is/fun/LisaNewClassYay.java"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testChangeClassPackage(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeClassPackage",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testChangeClassPackage"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the second package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.lol",
            "additionalParameters": ["testChangeClassPackage"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the class
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testChangeClassPackage","com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the class package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["package", "com.programcreek.is.fun", "com.programcreek.lol", "testChangeClassPackage"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Class NewClass.java moved successfully from package com.programcreek.is.fun to package com.programcreek.lol",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testChangeClassPackage/src/com/programcreek/lol/NewClass.java"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testDeleteClass(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testDeleteClass",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testDeleteClass"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the class
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testDeleteClass","com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # delete the class
        send_to_client = {
            "requestID": "lisa",
            "commandName": "delete",
            "objectType": "class",
            "objectName": "NewClass",
            "additionalParameters": ["testDeleteClass", "com.programcreek.is.fun"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Class NewClass.java deleted successfully",
                        "responseContent":""
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    ############################
    #  Folder related tests   #
    ############################

    def testCreateFolder(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testCreateFolder",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a random package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testCreateFolder"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["testCreateFolder","src/com/programcreek/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Folder Wahoo created in project testCreateFolder",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testCreateFolder/src/com/programcreek/Wahoo"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testDeleteFolder(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testDeleteFolder",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a random package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testDeleteFolder"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["testDeleteFolder","src/com/programcreek/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # delete the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "delete",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["testDeleteFolder", "src/com/programcreek/"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')


        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Folder Wahoo deleted in project testDeleteFolder",
                        "responseContent":""
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testFolderRename(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeFolderName",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a random package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testChangeFolderName"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["testChangeFolderName","src/com/programcreek/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the folder name
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["name", "testChangeFolderName", "src/com/programcreek/", "thesisRocks"]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')


        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Folder Wahoo renamed to thesisRocks in project testChangeFolderName",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testChangeFolderName/src/com/programcreek/thesisRocks"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testFolderChangeParent(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeFolderParent",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a random package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testChangeFolderParent"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["testChangeFolderParent","src/com/programcreek/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # change the folder parent
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "folder",
            "objectName": "Wahoo",
            "additionalParameters": ["parentFolder", "testChangeFolderParent", "src/com/programcreek/", ""]
        }

        # send the class creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')


        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"Folder Wahoo moved from src/com/programcreek/ to / in project testChangeFolderParent",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testChangeFolderParent/Wahoo"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    ############################
    #  File related tests   #
    ############################

    def testCreateFile(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testCreateFile",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a random package
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "package",
            "objectName": "com.programcreek.is.fun",
            "additionalParameters": ["testCreateFile"]
        }

        # send create package request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a readme file
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["testCreateFile","src/"]
        }

        # send the file creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                        "requestID":"lisa",
                        "responseCode":200,
                        "responseComment":"File readme.md created in project testCreateFile",
                        "responseContent":"/Users/Lisa/Desktop/workspace/testCreateFile/src/readme.md"
                    }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

        # create the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "file",
            "objectName": "helloWorld.txt",
            "additionalParameters": ["testCreateFile", "/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                          "requestID":"lisa",
                          "responseCode":200,
                          "responseComment":"File helloWorld.txt created in project testCreateFile",
                          "responseContent":"/Users/Lisa/Desktop/workspace/testCreateFile/helloWorld.txt"
                      }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testDeleteFile(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testDeleteFile",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a readme file
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["testDeleteFile","src/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # delete the folder
        send_to_client = {
            "requestID": "lisa",
            "commandName": "delete",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["testDeleteFile", "src/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                          "requestID":"lisa",
                          "responseCode":200,
                          "responseComment":"File readme.md deleted from project testDeleteFile",
                          "responseContent":""
                      }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testRenameFile(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testRenameFile",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a readme file
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["testRenameFile","src/"]
        }

        # send the file creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)


        # rename the file
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["name", "testRenameFile", "src/", "definitelyReadMe.md"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                          "requestID":"lisa",
                          "responseCode":200,
                          "responseComment":"File readme.md renamed to definitelyReadMe.md in project testRenameFile",
                          "responseContent":"/Users/Lisa/Desktop/workspace/testRenameFile/src/definitelyReadMe.md"
                      }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

    def testChangeFileParent(self):
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(('localhost', 9000))

        # create the project first
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "project",
            "objectName": "testChangeFileParent",
            "additionalParameters": []
        }

        # send create project request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)

        # create a readme file
        send_to_client = {
            "requestID": "lisa",
            "commandName": "create",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["testChangeFileParent","/"]
        }

        # send the file creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')
        client_socket.recv(1024)


        # change the file parent
        send_to_client = {
            "requestID": "lisa",
            "commandName": "change",
            "objectType": "file",
            "objectName": "readme.md",
            "additionalParameters": ["parentFolder", "testChangeFileParent", "/", "src/"]
        }

        # send the folder creation request to server
        client_socket.send(json.dumps(send_to_client) + '\n')

        expected_response = """{
                          "requestID":"lisa",
                          "responseCode":200,
                          "responseComment":"File readme.md moved from / to src/ in project testChangeFileParent",
                          "responseContent":"/Users/Lisa/Desktop/workspace/testChangeFileParent/src/readme.md"
                      }"""

        response_received = client_socket.recv(1024).decode('utf-8')
        self.assertEqual(jsoncompare.json_are_same(expected_response, response_received), True)

def main():
    unittest.main()

if __name__ == '__main__':
    main()